var app = angular.module('flickrApp', ['ionic']);

app.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider.state('home', {
    url: '/home',
    templateUrl: 'templates/home.html'
  });

  $stateProvider.state('albums', {
    url: '/albums',
    templateUrl: 'templates/albums.html'
  });

  $stateProvider.state('photos', {
    url: '/photos/:albumId',
    templateUrl: 'templates/photos.html'
  });

  $stateProvider.state('photo', {
    url: '/photo/:photoId',
    templateUrl: 'templates/photo.html'
  });

  $stateProvider.state('settings', {
    url: '/settings',
    templateUrl: 'templates/settings.html'
  });

  $urlRouterProvider.otherwise('/home');
});

app.controller("AlbumCtrl", function($scope, $http){
    $scope.albums = [];
    var albumParams = {
      method: 'flickr.photosets.getList',
      api_key: 'ca5545907d21c72639dd50478df32617',
      user_id: '150670880@N04',
      format: 'json',
      nojsoncallback: 1
    }
    $http.get('https://api.flickr.com/services/rest/', {params: albumParams})
      .success(function(response) {
        console.log("Flickr response: " + response);
        console.log(response);
        angular.forEach(response.photosets.photoset, function(child) {
          // each photoset (album) has a title and description property
          album = {
            id: child.id,
            title: child.title._content,
            description: child.description._content
          };
          $scope.albums.push(album);
        });
        console.log("Albums retrieved from flickr api:");
        console.log($scope.albums);
      })
      .error(function(response){
        console.log("Error calling flickr app. " + response);
      });
});

var photos = [];

var getPhoto = function(photoId){
  for(i=0; i < photos.length; i++){
    if(photoId == photos[i].id){
      return photos[i];
    }
  }
}

app.controller("PhotosCtrl", function($scope, $http, $stateParams){
    $scope.photos = [];
    var photosParams = {
      method: 'flickr.photosets.getPhotos',
      api_key: 'ca5545907d21c72639dd50478df32617',
      user_id: '150670880@N04',
      photoset_id: $stateParams.albumId,
      format: 'json',
      nojsoncallback: 1
    }
    $http.get('https://api.flickr.com/services/rest/', {params: photosParams})
      .success(function(response) {
        console.log("Flickr response: " + response);
        console.log(response);
        angular.forEach(response.photoset.photo, function(child) {
          photo = {
            id: child.id,
            secret: child.secret,
            server: child.server,
            farm: child.farm,
            title: child.title
          };
          photo.url = "https://farm" + photo.farm + ".staticflickr.com/" + photo.server + "/" + photo.id + "_" + photo.secret + ".jpg";
          $scope.photos.push(photo);
        });
        console.log("Photos retrieved from flickr api:");
        console.log($scope.photos);
        photos = $scope.photos;
      })
      .error(function(response){
        console.log("Error calling flickr app. " + response);
      });
});

app.controller("PhotoCtrl", function($scope, $http, $stateParams){
    var photo = getPhoto($stateParams.photoId);
    $scope.photo = photo;
    $scope.comments = [];

    var commentsParams = {
      method: 'flickr.photos.comments.getList',
      api_key: 'ca5545907d21c72639dd50478df32617',
      photo_id: $stateParams.photoId,
      format: 'json',
      nojsoncallback: 1
    }
    $http.get('https://api.flickr.com/services/rest/', {params: commentsParams})
      .success(function(response) {
        console.log("Flickr response: " + response);
        console.log(response);
        angular.forEach(response.comments.comment, function(child) {
          comment = {
            id: child.id,
            content: child._content,
            author: child.authorname,
            authorRealName: child.realname
          };
          $scope.comments.push(comment);
        });
        console.log("Comments retrieved from flickr api:");
        console.log($scope.comments);
      })
      .error(function(response){
        console.log("Error calling flickr app for comments. " + response);
      });

    $scope.openInBrowser = function(url) {
    window.open(url, '_blank');
  };  
});

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.cordova && window.cordova.InAppBrowser) {
      window.open = window.cordova.InAppBrowser.open;
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});