Trabajo Practico para la materia Desarrollo de Multiples Pantallas - UTN - 2017
Integrantes:
-Andrada, Emiliano
-Simoncelli, Fabricio
-Villalba, Facundo

----------------------------------------------------------------------------------------------------------------------
Prerequisitos y dependencias
Antes de instalar Cordova/PhoneGap es necesario instalar las siguientes dependencias:

Node.js
Cordova y PhoneGap son herramientas desarrolladas con Node.js. Para el presente curso, es necesario contar con Node.js version 4.4 o superior: https://nodejs.org/en/download/

GIT
GIT es usado por la herramienta de línea de comando de Cordova/PhoneGap para descargar dependencias. Si no está instalado en su PC, puede descargar la última versión de https://git-scm.com/.

Java Development Kit
Para desarrollar aplicaciones Android se require Java Development Kit 7 o superior. El mismo puede ser descargado de http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html.

Una vez instalado, es necesario definir la variable de entorno JAVA_HOME:

Abrir el Panel de Control y luego Sistema (alternativamente, puede ejecutar sysdm.cpl).
Seleccione la pestaña "Avanzado".
Presione en "Variables de Entorno".
Bajo la lista de "Variables de Sistema", presione "Nuevo...".
Especifique JAVA_HOME como nombre y el directorio donde instaló Java Development Kit como valor.
Configuración de JAVA_HOME

Por último, desde una nueva línea de comando, ejecute el siguiente comando para verificar que la instalación ha sido exitosa:

javac -version

el cual debería imprimir un mensaje similar al siguiente:

javac 1.8.0_40

Android Studio
Android Studio es utilizado para compilar y ejecutar aplicaciones Android. El mismo puede descargarse desde https://developer.android.com/studio/index.html.

Una vez instalado, agregar los siguientes directorios a la variable de entorno PATH:

tools (usualmente ubicado en C:\Users\{usuario}\AppData\Local\Android\sdk\tools)
platform-tools (usualmente ubicado en C:\Users\{usuario}\AppData\Local\Android\sdk\platform-tools)

----------------------------------------------------------------------------------------------------------------------